<?php
//error_reporting(E_ALL ^ E_NOTICE);
function chars_specials( $str ){

    $str = str_replace( '&quot;', '"', $str );
    $str = str_replace( '&nbsp;', ' ', $str );
    $str = str_replace( '&ldquo;', '', $str );
    $str = str_replace( '&rdquo;', '', $str );
    $str = str_replace( "&lsquo;", "'", $str );
    $str = str_replace( "&rsquo;", "'", $str );
    $str = str_replace( "?", "", $str );
    return $str;
}

if (isset($_REQUEST['formatvid']) && $_REQUEST['formatvid'] != "") {
    $forVid = $_REQUEST['formatvid'];
}else{
    $forVid = 'apps';
}

if(isset($_REQUEST['section']) && $_REQUEST['section'] != ""){
    $seccion = $_REQUEST['section'];
}else{
    $seccion = "home";
}
switch ($seccion) {
    case 'copa-america':
        $req = 'http://deportes.televisa.com/content/televisa/deportes/video/copa-america.video.js';
        $sufijo= "MICRCA";                                          // micrositio - Repositorio copa Am�rica
        break;
    case 'copa-oro':
        $req = 'http://deportes.televisa.com/content/televisa/deportes/video/copa-oro.video.js';
        $sufijo = "MICRCO";                                         //micrositio - Repositorio copa Oro
        break;
	default:
        $req = 'http://deportes.televisa.com/content/televisa/deportes.mvideo.js';
 }

$dom_feed= explode("content/televisa", $req);
$dominio= trim($dom_feed[0], "/");
$subdominio= array("m4v.","media.","flash.");
$cURL = curl_init($req);
curl_setopt($cURL,CURLOPT_RETURNTRANSFER, TRUE);
$app_deo = curl_exec($cURL);
//$app_deo = utf8_encode($app_deo);
$deportes_app = json_decode($app_deo, true);
#echo "<pre>"; print_r($deportes_app); echo "</pre>"; die();
$array_feed= array();

if(isset($deportes_app['category'])){

    foreach($deportes_app['category'] as $key => $cat){
        foreach($cat['program'] as $b => $prog){
            foreach($prog['videos'] as $key => $val){
                $array_feed[$key]['title']= $val['title'];

                $url_video= "";

                if(isset($val['urls'][$forVid]) && $val['urls'][$forVid] != ""){
                    $url_video= $val['urls'][$forVid];
                }
                $ur_vid_aux= explode("?", $url_video);
                if(count($ur_vid_aux > 1)){
                    $url_video= $ur_vid_aux[0];
                }
                $url_video= strtolower($url_video);
                $url_video= str_replace($subdominio, 'apps.',$url_video);
                if(isset($val['description']) && $val['description'] != ""){
                    $description= $val['description'];
                }else{
                    $description= $val['title'];
                }
                if(isset($val['publidate']) && $val['publidate'] != ""){
                    $date_pub= date('Y-m-d H:i:s ',strtotime($val['publidate']));
                }else{
                    $date_pub= date("Y-m-d H:i:s");
                }

                if(isset($val['duration']) && $val['duration'] != ""){
                    $duration= $val['duration'];
                }else{
                    $duration= "10";
                }
                if(isset($val['still']) && $val['still'] != ""){
                    $still= $val['still'];
                }else{
                    $still= $val['thumb'];
                }
                $val_still= strpos($still, 'http://');
                if($val_still === false){
                    $still= $dominio.$still;
                }
                $sepurl_still= explode("?", $still);
                if(count($sepurl_still) > 1){
                    $still= $sepurl_still[0];
                    $still= str_replace($subdominio, 'apps.',$still);
                }

                $thumb= $val['thumb'];
                $val_thumb= strpos($thumb, 'http://');
                if($val_thumb === false){
                    $thumb= $dominio.$thumb;
                }
                $sepurl_thumb= explode("?", $thumb);            //limpiando url de thumb
                if(count($sepurl_thumb) > 1){
                    $thumb= $sepurl_thumb[0];
                    $thumb= str_replace($subdominio, 'apps.',$thumb);
                }
                $array_feed[$key]['link']=  $val['urlpublic'];
                $array_feed[$key]['description']= $description;
                $array_feed[$key]['pubDate']= $date_pub;
                $array_feed[$key]['pubDateTime']= $date_pub;
                $array_feed[$key]['url_video']= $url_video;
                $array_feed[$key]['duration']= $duration;
                $array_feed[$key]['program']= 'deportes';
                $array_feed[$key]['role']= 'deportes';
                $array_feed[$key]['path']= $val['path'];
                $array_feed[$key]['keywords']= $val['keywords'];
                $array_feed[$key]['thumbnail']= $thumb;
                $array_feed[$key]['still'] = $still;
                $array_feed[$key]['programid']= 0129;
                $array_feed[$key]['video_id']= $val['id'].$sufijo;
                $array_feed[$key]['guid']= $val['guid'];

            }
        }
    }

}
//echo "<pre>"; print_r($array_feed);echo "</pre>";die();

header("Content-type: text/xml; charset=ISO-8859-1");
echo '<'.'?xml version="1.0" encoding="ISO-8859-1"?'.'>';
echo "\n";

?>
    <resources>
<?php
$elements_feed= 0;
foreach($array_feed as $key_item => $value_element){
    if($value_element['url_video'] != ""){
        if($elements_feed < 9){    ?>
	    <resource>
            <type>video</type>
		    <attributes>
		        <title><![CDATA[<?=chars_specials(utf8_decode($value_element['title']));?>]]></title>
		        <description><![CDATA[<?=chars_specials(utf8_decode($value_element['description']));?>]]></description>
		        <external_id><?=$value_element['video_id']?></external_id>
                <pubDate><?=$value_element['pubDate']?></pubDate>
                <enable>true</enable>
		        <alternative_streams>
		        <default><?php echo $value_element['url_video']?></default>
		        </alternative_streams>
		        <image_assets>
                <image_asset>
                    <key>item_small</key>
                    <url><?=$value_element['thumbnail'];?></url>
                </image_asset>
                <image_asset>
                    <key>item_big</key>
                    <url><?=$value_element['still'];?></url>
                </image_asset>
            </image_assets>
		    </attributes>
	    </resource>
<?php   }
        $elements_feed++;
    }
}
echo "</resources>";
?>