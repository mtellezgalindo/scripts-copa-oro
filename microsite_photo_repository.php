<?php
ini_set("max_execution_time", "0");

/*
----- funciones
*/
function chars_specials( $str ){

  $str = str_replace( '&quot;', '"', $str ); 
  $str = str_replace( '&nbsp;', ' ', $str );  
  $str = str_replace( '&ldquo;', '', $str );
  $str = str_replace( '&rdquo;', '', $str );
  $str = str_replace( "&lsquo;", "'", $str );
  $str = str_replace( "&rsquo;", "'", $str );
  $str = str_replace( "?", "", $str );
  return $str; 
}	

function convert_chars_to_entities( $str ) 
{ 
    $str = str_replace( 'ÃƒÂ¡', '&aacute;', $str ); 
    $str = str_replace( 'ÃƒÂ©', '&eacute;', $str ); 
    $str = str_replace( 'ÃƒÂ­', '&iacute;', $str ); 
    $str = str_replace( 'ÃƒÂ³', '&oacute;', $str ); 
    $str = str_replace( 'ÃƒÂº', '&uacute;', $str ); 
    $str = str_replace( 'ÃƒÂ±', '&ntilde;', $str ); 
    //$str = str_replace( '"', '&quot;', $str ); 
    $str = str_replace( 'ÃƒÂƒÃ‚Â¡' , '&aacute;', $str ); 
    $str = str_replace( 'ÃƒÂƒÃ‚Â©' , '&eacute;', $str ); 
    $str = str_replace( 'ÃƒÂƒÃ‚Â³' , '&oacute;', $str ); 
    $str = str_replace( 'ÃƒÂƒÃ‚Âº' , '&uacute;', $str ); 
    $str = str_replace( 'ÃƒÂƒÃ¢Â€Â°', '&Eacute;', $str ); 
    $str = str_replace( 'ÃƒÂƒÃ‚Â±', '&ntilde;', $str ); 
    $str = str_replace( 'ÃƒÂƒ-', '&iacute;', $str ); 
    $str = str_replace( 'ci?', 'ci&oacute;n', $str ); 
    $str = str_replace( 'ÃƒÂ', '&Aacute;', $str ); 
    $str = str_replace( 'ÃƒÂ‰', '&Eacute;', $str ); 
    $str = str_replace( 'ÃƒÂ', '&Iacute;', $str ); 
    $str = str_replace( 'ÃƒÂ“', '&Oacute;', $str ); 
    $str = str_replace( 'ÃƒÂš', '&Uacute;', $str ); 
    $str = str_replace( 'ÃƒÂ‘', '&Ntilde;', $str ); 
    $str = str_replace( 'aÃƒÂ¯Ã‚Â¿Ã‚Â½a', 'a&ntilde;a', $str ); 
    $str = str_replace( 'aÃƒÂ¯Ã‚Â¿Ã‚Â½e', 'a&ntilde;e', $str ); 
    $str = str_replace( 'aÃƒÂ¯Ã‚Â¿Ã‚Â½i', 'a&ntilde;i', $str ); 
    $str = str_replace( 'aÃƒÂ¯Ã‚Â¿Ã‚Â½o', 'a&ntilde;o', $str ); 
    $str = str_replace( 'aÃƒÂ¯Ã‚Â¿Ã‚Â½u', 'a&ntildeu', $str ); 
    $str = str_replace( 'eÃƒÂ¯Ã‚Â¿Ã‚Â½a', 'e&ntilde;a', $str ); 
    $str = str_replace( 'eÃƒÂ¯Ã‚Â¿Ã‚Â½e', 'e&ntilde;e', $str ); 
    $str = str_replace( 'eÃƒÂ¯Ã‚Â¿Ã‚Â½i', 'e&ntildei', $str ); 
    $str = str_replace( 'eÃƒÂ¯Ã‚Â¿Ã‚Â½u', 'e&ntildeu', $str ); 
    $str = str_replace( 'iÃƒÂ¯Ã‚Â¿Ã‚Â½a', 'i&ntilde;a', $str ); 
    $str = str_replace( 'iÃƒÂ¯Ã‚Â¿Ã‚Â½e', 'i&ntilde;e', $str ); 
    $str = str_replace( 'iÃƒÂ¯Ã‚Â¿Ã‚Â½i', 'i&ntilde;i', $str ); 
    $str = str_replace( 'iÃƒÂ¯Ã‚Â¿Ã‚Â½o', 'i&ntilde;o', $str ); 
    $str = str_replace( 'iÃƒÂ¯Ã‚Â¿Ã‚Â½u', 'i&ntilde;u', $str ); 
    $str = str_replace( 'oÃƒÂ¯Ã‚Â¿Ã‚Â½a', 'o&ntilde;a', $str ); 
    $str = str_replace( 'oÃƒÂ¯Ã‚Â¿Ã‚Â½e', 'o&ntilde;e', $str ); 
    $str = str_replace( 'oÃƒÂ¯Ã‚Â¿Ã‚Â½i', 'o&ntilde;i', $str ); 
    $str = str_replace( 'oÃƒÂ¯Ã‚Â¿Ã‚Â½u', 'o&ntilde;u', $str ); 
    $str = str_replace( 'uÃƒÂ¯Ã‚Â¿Ã‚Â½a', 'u&ntilde;a', $str ); 
    $str = str_replace( 'uÃƒÂ¯Ã‚Â¿Ã‚Â½e', 'u&ntilde;e', $str ); 
    $str = str_replace( 'uÃƒÂ¯Ã‚Â¿Ã‚Â½i', 'u&ntilde;i', $str );
    $str = str_replace( 'uÃƒÂ¯Ã‚Â¿Ã‚Â½o', 'u&ntilde;o', $str );
    $str = str_replace( 'uÃƒÂ¯Ã‚Â¿Ã‚Â½u', 'u&ntilde;u', $str );
    $str = str_replace( 'iÃƒÂ¯Ã‚Â¿Ã‚Â½n', 'i&oacute;n', $str );
    $str = str_replace( 'Ã¡', '&aacute;', $str ); 
    $str = str_replace( 'Ã©', '&eacute;', $str ); 
    $str = str_replace( 'Ã­', '&iacute;', $str ); 
    $str = str_replace( 'Ã³', '&oacute;', $str ); 
    $str = str_replace( 'Ãº', '&uacute;', $str ); 
    $str = str_replace( 'Ã±', '&ntilde;', $str );
    $str = str_replace( 'Ã¼', '&uuml;', $str );  //#latin small letter "ü" 
    $str = str_replace( 'Ã', '&Aacute;', $str ); //Ã con tilde
    $str = str_replace( 'Ãš', '&Uacute;', $str ); //U con tilde   

   
    return $str; 
}


if(isset($_REQUEST['section']) && $_REQUEST['section'] != ""){
    $seccion = $_REQUEST['section'];    
}else{
    $seccion = "home";
}
switch ($seccion) {
	case 'copa-america':
		$req = 'http://deportes.televisa.com/content/televisa/deportes/fotogalerias/copa-america.photo.js';
		break;
    case 'copa-oro':
        $req = "http://deportes.televisa.com/content/televisa/deportes/fotogalerias/copa-oro.photo.js";
        break;
	case 'home':
		$req = 'http://deportes.televisa.com/content/televisa/deportes.mix.js';
		break;	
}	

$tipo = 'photo';
$deportes_app= array();
$cURL = curl_init($req);
curl_setopt($cURL,CURLOPT_RETURNTRANSFER, TRUE);
$data = curl_exec($cURL);
#$data= utf8_encode($data);
$deportes_app = json_decode($data, true);
//echo "<pre>"; print_r($deportes_app); echo"</pre>"; die();
$array_feed_photo= array();	

if(count($deportes_app) > 0){
    foreach($deportes_app['items'] as $key => $val){
    	   if ($val['typeElement']=="photo" && $tipo=="photo"){
    			   $array_feed_photo[$key]['title']= $val['title'];
    			   $array_feed_photo[$key]['typeElement']= $val['typeElement'];
    			   
    			   $rlink = strpos($val['link'], 'http://');
    			   if($rlink !== false){
    					$link = $val['link']; 
    			   } else {
    					$link = $val['link'];                         
    			   }  			   
    			   $array_feed_photo[$key]['link'] = $link;
    			   #$array_feed_photo[$key]['description'] = $val['description'];
    			   if ( !isset($val['pubDate']) || $val['pubDate'] == '' || $val['pubDate'] == NULL || empty($val['pubDate']) ){
    				   $pubDate = date('D, d M Y H:i:s ',strtotime(date("Y/m/d")));		
    			   } else {
    				   $pubDate = date('D, d M Y H:i:s ',strtotime($val['pubDate']));
    			   }			   
    			   $array_feed_photo[$key]['pubDate'] = $pubDate;
    			   $rthum = strpos($val['thumbnail'], 'http://');
    			   if($rthum !== false){
    					$thumb = $val['thumbnail']; 
    			   } else {
    					$thumb = $val['thumbnail'];
    			   } 			   
    			   $array_feed_photo[$key]['thumbnail'] = $thumb; 
    			   $array_feed_photo[$key]['photos'] = $val['photo'];
    			   $array_feed_photo[$key]['guid'] = $val['guid'];
    			   $porciones = explode(" ", $val['description']);
    			   $p="";
                   
                   if(isset($val['description']) && $val['description'] != ""){
                       foreach($porciones as $porcion){
    		               $v = convert_chars_to_entities($porcion);
    			           $p = $p . " ". $v;
                       }
    	               $array_feed_photo[$key]['description'] = $p;
       	           }else{
                  	   $array_feed_photo[$key]['description']= $val['title'];
         	       }
    			   
    	   }
    }//for
}    
header("Content-type: text/xml; charset=ISO-8859-1");
echo '<'.'?xml version="1.0" encoding="ISO-8859-1"?'.'> <rss version="2.0" 
xmlns:content="http://purl.org/rss/1.0/modules/content/"
xmlns:wfw="http://wellformedweb.org/CommentAPI/"
xmlns:dc="http://purl.org/dc/elements/1.1/"
xmlns:media="http://search.yahoo.com/mrss/"
xmlns:atom="http://www.w3.org/2005/Atom">';
echo "\n";

	   	echo "<channel>";
	   	echo "<title>televisa.com</title>";
	   	echo "<link>http://www.televisa.com</link>";
	   	echo "<description>".utf8_decode('El sitio número de internet de habla hispana con el mejor contenido de noticias, espectáculos, telenovelas, deportes, futbol, estadísticas y mucho más.')."</description>";
	   	echo "<image>";
	   	    echo "<title>televisa.com</title>";
	   	    echo "<url>http://i.esmas.com/img/univ/portal/rss/feed_1.jpg</url>";
	   	    echo "<link>http://www.televisa.com</link>";
	       echo "</image>";
	       echo "<language>es-mx</language>";
	   	echo "<copyright>2005 Comercio Mas S.A. de C.V</copyright>";
	   	echo "<managingEditor>ulises.blanco@esmas.net (Ulises Blanco)</managingEditor>";
	   	echo "<webMaster>feeds@esmas.com (feeds Esmas.com)</webMaster>";
	   	echo "<pubDate>".date("D, d M Y H:i:s")." GMT</pubDate>";
	   	echo "<lastBuildDate>".date("D, d M Y H:i:s")." GMT</lastBuildDate>";
	   	echo "<category>Home Principal esmas</category> ";
	   	echo "<generator>GALAXY 1.0</generator>";
	   	echo '<atom:link href="http://feeds.esmas.com/data-feeds-esmas/xml/index.xml" rel="self" type="application/rss+xml" />';
	   	echo "<ttl>60</ttl> ";

if (count($array_feed_photo) <> 0 && $tipo == "photo"){
  foreach($array_feed_photo as $key_item => $value_element){
   if(isset($value_element['photos']) && count($value_element['photos']) > 0 && $value_element['photos'] <> 'empty' && $value_element['photos'] <> '' && $value_element['photos'] <> NULL ){
?>  
		<item>
	        <title><![CDATA[<?=chars_specials(utf8_decode($value_element['title']))?>]]></title>
            <link><?=$value_element['link']?></link>
            <description><![CDATA[<?=html_entity_decode(convert_chars_to_entities(utf8_decode($value_element['description'])))?>]]></description>
			<pubDate><?=trim($value_element['pubDate']);?> CDT</pubDate>
			<media:thumbnail url='<?=utf8_decode($value_element['thumbnail'])?>' /> 
<?php
foreach($value_element['photos'] as $k => $valor){
  if(isset($valor['images']) && count($valor['images']) > 0){
      if(isset($valor['title']) &&  $valor['title'] <> "" && $valor['title'] <> NULL){
         $title_gal = $valor['title'];
      }else{
         $title_gal = "...";
      }?>	 
            <media:group>
	          <media:title><![CDATA[<?=chars_specials(utf8_decode($title_gal))?>]]></media:title>			 
<?php
     	        $images = $valor['images'];
			    foreach($images as $m => $imagen){
				   $p = strpos($m, 'x', 0);
				   $h = substr($m,$p+1); 
				   $w = substr($m,0,$p);
			       $rimg = strpos($images[$m], 'http://');
			       if($rimg !== false){ $imagen = $images[$m]; } else {  $imagen = $url.$images[$m]; }?>
                    <media:content url='<?=utf8_decode($imagen)?>' medium='image' width='<?=$w?>' height='<?=$h?>' />
                <?php
				}
				?>			 
			</media:group>	
<?php 
    }
}
?>		<guid isPermaLink='false'><?=$value_element['guid']?></guid>			 
        </item>		
<?php
   }	
  }
}
  
	echo "</channel>";
	echo "</rss>";
 	
?>