<?php
//error_reporting(E_ALL ^ E_NOTICE);
date_default_timezone_set('America/Mexico_City');
/*
----- funciones
*/
function chars_specials( $str ){

  $str = str_replace( '&quot;', '"', $str ); 
  $str = str_replace( '&nbsp;', ' ', $str );  
  $str = str_replace( '&ldquo;', '', $str );
  $str = str_replace( '&rdquo;', '', $str );
  $str = str_replace( "&lsquo;", "'", $str );
  $str = str_replace( "&rsquo;", "'", $str ); 
  
  return $str; 
}

if(isset($_REQUEST['section']) && $_REQUEST['section'] != ""){
    $seccion = $_REQUEST['section'];    
}else{
    $seccion = "home";
}
switch ($seccion) {
	case 'copa_america':
		$req_mix = 'http://deportes.televisa.com/content/televisa/deportes/copa-america.mix.js';
        break;
  case 'copa-oro':
    $req_mix = 'http://deportes.televisa.com/content/televisa/deportes/copa-oro.mix.js';
      break;
	default:
        $req_mix = 'http://deportes.televisa.com/content/televisa/deportes.mix.js';
    
}
$dom_feed= explode("content/televisa", $req_mix);
$dominio= trim($dom_feed[0], "/");
$videopoll = "";

$cURL = curl_init($req_mix);
curl_setopt($cURL,CURLOPT_RETURNTRANSFER, TRUE);
$app_deo = curl_exec($cURL);
//$app_deo = utf8_encode(curl_exec($cURL));
$deportes_app = json_decode($app_deo, true); 
//echo "<pre>"; print_r($deportes_app); echo "</pre>"; die();

function complete_url($img_size,$dominio){
    $img_url= strpos($img_size, 'http://');
    if($img_url === false){
        $new_url= $dominio.$img_size;
    }else{
        $new_url= $img_size;
    }
    return($new_url);
}
function checkpoll($link){
    $videoencuesta_url= strpos($link, 'videoencuesta');
    if($videoencuesta_url === false){
        return "noexiste";
    }else{
        return "existe";
    }
}
$array_feed= array();
foreach($deportes_app['items'] as $key => $val){
    $flag_state_note = 1;
    //echo "<b>".$key." - ".$val['link']."<b><br>";
    if( isset($val['typeElement']) && $val['typeElement'] == "article"){    //Tipo de item ::: article
        
        $videopoll = checkpoll($val['link']);
        if($videopoll == "existe"){
            $flag_state_note= 0;                                            //echo "<b>Es una nota de encuesta</b><br>";
        }
        
        /*-------------------------------------     
                            Validando notas MxM
                                            --------------------------------------*/            
        
        $aux_mxm_search= strpos($val['link'], "/mxm/futbol/");
        if($aux_mxm_search !== false){                                      //echo "<b>Es una nota del MXM</b><br>";
            $size_content= strlen($val['content']);                 
            if($val['content'] == "" || $size_content < 100){
                $flag_state_note= 0;                                
            }
        }
        /*-------------------------------------     
                            Validando notas en vivo
                                            --------------------------------------*/           
        $aux_live_search= strpos($val['link'], "/video/vivo/");
        if($aux_live_search !== false){                                     //echo "<b>Es una nota de evento en VIVO</b><br>";
                $flag_state_note= 0;                                
        }        
        
        
        if($flag_state_note== 1){
    
            $thumb= $val['thumbnail'];
            $val_thumb= strpos($thumb, 'http://');
            if($val_thumb === false){
                $thumb= $dominio.$thumb;
            }
            if(isset($val['pubDate']) && $val['pubDate'] != ""){
            	$date_pub= date('D, d M Y H:i:s ',strtotime($val['pubDate']));
           	}else{
            	$date_pub= date("D, d M Y H:i:s");
           	}
            if(isset($val['keywords']) && $val['keywords'] != ""){
            	$keywords= $val['keywords'];
           	}else{
            	$keywords= "Televisa Deportes";
           	}
            $img_1024x768= complete_url($val['images']['1024x768'],$dominio);
            $img_110x110= complete_url($val['images']['110x110'],$dominio);
            $img_136x102= complete_url($val['images']['136x102'],$dominio);
            $img_136x104= complete_url($val['images']['136x104'],$dominio);
            $img_136x77= complete_url($val['images']['136x77'],$dominio);
            $img_192x107= complete_url($val['images']['192x107'],$dominio);
            $img_210x120= complete_url($val['images']['210x120'],$dominio);
            $img_300x169= complete_url($val['images']['300x169'],$dominio);
            $img_319x319= complete_url($val['images']['319x319'],$dominio);
            $img_408x306= complete_url($val['images']['408x306'],$dominio);
            $img_48x48= complete_url($val['images']['48x48'],$dominio);
            $img_624x351= complete_url($val['images']['624x351'],$dominio);
            $img_624x468= complete_url($val['images']['624x468'],$dominio);
            $img_63x63= complete_url($val['images']['63x63'],$dominio);
            $img_84x63= complete_url($val['images']['84x63'],$dominio);
            $img_84x47= complete_url($val['images']['84x47'],$dominio);
            $img_67x38= complete_url($val['images']['67x38'],$dominio);
                       
            $array_feed[$key]['title']= $val['title'];
            $array_feed[$key]['link'] = $val['link'];
            $array_feed[$key]['authorname'] = $val['authorname'];
            $array_feed[$key]['description'] = $val['description'];
            $array_feed[$key]['pubDate'] = $date_pub;
            $array_feed[$key]['thumb'] = $thumb;
            $array_feed[$key]['images']['1024x768'] = $img_1024x768;
            $array_feed[$key]['images']['110x110'] = $img_110x110;
            $array_feed[$key]['images']['136x102'] = $img_136x102;
            $array_feed[$key]['images']['136x104'] = $img_136x104;
            $array_feed[$key]['images']['136x77'] = $img_136x77;
            $array_feed[$key]['images']['192x107'] = $img_192x107;
            $array_feed[$key]['images']['210x120'] = $img_210x120;
            $array_feed[$key]['images']['300x169'] = $img_300x169;
            $array_feed[$key]['images']['319x319'] = $img_319x319;
            $array_feed[$key]['images']['408x306'] = $img_408x306;
            $array_feed[$key]['images']['48x48'] = $img_48x48;
            $array_feed[$key]['images']['624x351'] = $img_624x351;
            $array_feed[$key]['images']['624x468'] = $img_624x468;
            $array_feed[$key]['images']['63x63'] = $img_63x63;
            $array_feed[$key]['images']['84x63'] = $img_84x63;
            $array_feed[$key]['images']['84x47'] = $img_84x47;
            $array_feed[$key]['images']['67x38'] = $img_67x38;
            $array_feed[$key]['content'] = $val['content'];
            $array_feed[$key]['guid'] = $val['guid'];
            $array_feed[$key]['keywords'] = $keywords;
            $array_feed[$key]['comments'] = $val['comments'];
            $array_feed[$key]['category'] = $val['category'];
        }
    }            
   
}

//echo "<pre>"; print_r($array_feed);echo "</pre>";die(); 
      #die(); 
header("Content-type: text/xml; charset=ISO-8859-1");
echo '<'.'?xml version="1.0" encoding="ISO-8859-1"?'.'><rss version="2.0" 
xmlns:content="http://purl.org/rss/1.0/modules/content/"
xmlns:wfw="http://wellformedweb.org/CommentAPI/"
xmlns:dc="http://purl.org/dc/elements/1.1/"
xmlns:media="http://search.yahoo.com/mrss/"
xmlns:atom="http://www.w3.org/2005/Atom">';
?>
<channel>
    <title>televisa.com</title>
    <link>http://www.televisa.com</link>
    <description><?=utf8_decode('El sitio número de internet de habla hispana con el mejor contenido de noticias, espectáculos, telenovelas, deportes, futbol, estadísticas y mucho más.')?></description>
    <image>
        <title>televisa.com</title>
        <url>http://i.esmas.com/img/univ/portal/rss/feed_1.jpg</url>
        <link>http://www.televisa.com</link>
    </image>
    <language>es-mx</language>
    <copyright>2005 Comercio Mas S.A. de C.V</copyright>
    <managingEditor>ulises.blanco@esmas.net (Ulises Blanco)</managingEditor>
    <webMaster>feeds@esmas.com (feeds Esmas.com)</webMaster>
    <pubDate><?=date("D, d M Y H:i:s")?></pubDate>
    <lastBuildDate><?=date("D, d M Y H:i:s")?></lastBuildDate>
    <category>Home Principal esmas</category>
    <generator>GALAXY 1.0</generator>
    <atom:link href="http://feeds.esmas.com/data-feeds-esmas/xml/index.xml" rel="self" type="application/rss+xml" />
    <ttl>60</ttl>
<?php foreach($array_feed as $key_item => $value_element){?>
   <item>
    <?php if($seccion != "home"){ ?>
        <typeElement><![CDATA[<?="articulo-composicion"?>]]></typeElement>
    <?php } ?>
    	<title><![CDATA[<?=iconv("UTF-8", "ISO-8859-1//TRANSLIT",$value_element['title']); ?>]]></title>
   		<link><?=$value_element['link'];?></link>
   		<author><![CDATA[<?=iconv("UTF-8", "ISO-8859-1//TRANSLIT",$value_element['authorname']);?>]]></author>
   		<description><![CDATA[<?=iconv("UTF-8", "ISO-8859-1//TRANSLIT", $value_element['description']);?>]]></description>   		
   		<pubDate><?=trim($value_element['pubDate']);?> CDT</pubDate>
   	 	<media:thumbnail url='<?=$value_element['thumb'];?>'/>
   	 	<media:group>
   	 		 <media:content url='<?=$value_element['images']['1024x768'];?>' medium='image' height='768' width='1024' />
   	 		 <media:content url='<?=$value_element['images']['110x110'];?>' medium='image' height='110' width='110' />
   	 		 <media:content url='<?=$value_element['images']['136x102'];?>' medium='image' height='102' width='136' />
   	 		 <media:content url='<?=$value_element['images']['136x104'];?>' medium='image' height='104' width='136' />
   	 		 <media:content url='<?=$value_element['images']['136x77'];?>' medium='image' height='77' width='136' />
   	 		 <media:content url='<?=$value_element['images']['192x107'];?>' medium='image' height='107' width='192' />
   	 		 <media:content url='<?=$value_element['images']['210x120'];?>' medium='image' height='120' width='210' />
   	 		 <media:content url='<?=$value_element['images']['300x169'];?>' medium='image' height='169' width='300' />
   	 		 <media:content url='<?=$value_element['images']['319x319'];?>' medium='image' height='319' width='319' />
   	 		 <media:content url='<?=$value_element['images']['408x306'];?>' medium='image' height='306' width='400' />
   	 		 <media:content url='<?=$value_element['images']['48x48'];?>' medium='image' height='48' width='48' />
   	 		 <media:content url='<?=$value_element['images']['624x351'];?>' medium='image' height='351' width='624' />
   	 		 <media:content url='<?=$value_element['images']['624x468'];?>' medium='image' height='468' width='624' />
   	 		 <media:content url='<?=$value_element['images']['63x63'];?>' medium='image' height='63' width='63' />
   	 		 <media:content url='<?=$value_element['images']['84x63'];?>' medium='image' height='63' width='84' />
   	 		 <media:content url='<?=$value_element['images']['84x47'];?>' medium='image' height='74' width='84' />
   	 		 <media:content url='<?=$value_element['images']['67x38'];?>' medium='image' height='38' width='67' />
   	 	</media:group>
   	 	<content:encoded><![CDATA[<?=utf8_decode(chars_specials(html_entity_decode($value_element['content'])))?>]]></content:encoded>
        <guid isPermaLink='false'><?=$value_element['guid']; ?></guid>
   	 	<media:keywords><![CDATA[<?=iconv("UTF-8", "ISO-8859-1//TRANSLIT",$value_element['keywords']);?>]]></media:keywords>
   	 	<comments>http://comentarios.esmas.com/feeds/comentarios.php?url=<?=$value_element['link']; ?></comments>
   	 	<category><?=$value_element['category'];?></category>
   </item>        
<?php    }  ?>	   
    </channel>
</rss>