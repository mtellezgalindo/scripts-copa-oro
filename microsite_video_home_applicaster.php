<?php
//error_reporting(E_ALL ^ E_NOTICE);
function chars_specials( $str ){

    $str = str_replace( '&quot;', '"', $str );
    $str = str_replace( '&nbsp;', ' ', $str );
    $str = str_replace( '&ldquo;', '', $str );
    $str = str_replace( '&rdquo;', '', $str );
    $str = str_replace( "&lsquo;", "'", $str );
    $str = str_replace( "&rsquo;", "'", $str );
    $str = str_replace( "?", "", $str );
    return $str;
}

if(isset($_REQUEST['section']) && $_REQUEST['section'] != ""){
    $seccion = $_REQUEST['section'];
}else{
    $seccion = "home";
}
if(isset($_REQUEST['formatvid']) && $_REQUEST['formatvid'] != ""){
    $formatvid = $_REQUEST['formatvid'];
}else{
    $formatvid = "apps";
}
switch ($seccion) {
    case 'copa-america':
        $req_vid = 'http://deportes.televisa.com/content/televisa/deportes/copa-america.mix.js';
        $req_carousel= 'http://deportes.televisa.com/content/televisa/deportes/copa-america.mvideo.js';
        $sufijo = "MICCOA";                                              // micrositio copa Am�rica
        break;
    case 'copa-oro':
        $req_vid ="http://deportes.televisa.com/content/televisa/deportes/copa-oro.mix.js";
        $req_carousel ="http://deportes.televisa.com/content/televisa/deportes/copa-oro.mvideo.js";
        $sufijo = "MICCOO";                                             //micrositio copa Oro
        break;
	default:
        $req_vid = 'http://deportes.televisa.com/content/televisa/deportes.mvideo.js';
        
}
$dom_feed= explode("content/televisa", $req_vid);
$dominio= trim($dom_feed[0], "/");
$subdominio= array("m4v.","media.","flash.");

$cURL = curl_init($req_vid);
curl_setopt($cURL,CURLOPT_RETURNTRANSFER, TRUE);
$app_deo = curl_exec($cURL);
//$app_deo = utf8_encode(curl_exec($cURL));
$deportes_app = json_decode($app_deo, true);                                //echo "<pre>"; print_r($deportes_app); echo "</pre>";die();

$cURL_car = curl_init($req_carousel);
curl_setopt($cURL_car,CURLOPT_RETURNTRANSFER, TRUE);
$app_car = curl_exec($cURL_car);
//$app_car = utf8_encode(curl_exec($cURL_car));
$deportes_app_car = json_decode($app_car, true);                            //echo "<pre>"; print_r($deportes_app_car); echo "</pre>";die();

$array_feed= array();
$i= 0;
//RECORRIENDO MIX
if($deportes_app['items'] != null || $deportes_app['items'] != ""){
    foreach($deportes_app['items'] as $key => $val){
        if($val["typeElement"] == "video"){
            if(isset($val['urls'][$formatvid]) && $val['urls'][$formatvid] != ""){
                $url_video= $val['urls'][$formatvid];
                //$url_video= $val['urls']["apps"];                    
                $ur_vid_aux= explode("?", $url_video);
                if(count($ur_vid_aux > 1)){
                    $url_video= $ur_vid_aux[0];
                }
                $url_video= strtolower($url_video);
                $url_video= str_replace($subdominio, 'apps.',$url_video);

                if(isset($val['description']) && $val['description'] != ""){
                    $description= $val['description'];
                }else{
                    $description= $val['title'];
                }
                if(isset($val['publidate']) && $val['publidate'] != ""){
                    $date_pub= date('Y-m-d H:i:s ',strtotime($val['publidate']));
                }else{
                    $date_pub= date("Y-m-d H:i:s");
                }
                if(isset($val['duration']) && $val['duration'] != ""){
                    $duration= $val['duration'];
                }else{
                    $duration= "10";
                }
                if(isset($val['still']) && $val['still'] != ""){
                    $still= $val['still'];
                }else{
                    $still= $val['thumb'];
                }
                $val_still= strpos($still, 'http://');
                if($val_still === false){
                    $still= $dominio.$still;
                }
                $sepurl_still= explode("?", $still);
                if(count($sepurl_still) > 1){
                    $still= $sepurl_still[0];
                    $still= str_replace($subdominio, 'apps.',$still);
                }
                $thumb= $val['thumb'];
                $val_thumb= strpos($thumb, 'http://');
                if($val_thumb === false){
                    $thumb= $dominio.$thumb;
                }
                $sepurl_thumb= explode("?", $thumb);
                if(count($sepurl_thumb) > 1){
                    $thumb= $sepurl_thumb[0];
                    $thumb= str_replace($subdominio, 'apps.',$thumb);
                }
                $array_feed[$i]['section']= "mix";
                $array_feed[$i]['ind_feed']= $key;
                $array_feed[$i]['video_id']= $val['id'].$sufijo;
                $array_feed[$i]['title']= $val['title'];
                $array_feed[$i]['link']=  $val['urlpublic'];
                $array_feed[$i]['description']= $description;
                $array_feed[$i]['pubDate']= $date_pub;
                $array_feed[$i]['pubDateTime']= $date_pub;
                $array_feed[$i]['url_video']= $url_video;
                $array_feed[$i]['duration']= $duration;
                $array_feed[$i]['program']= 'deportes';
                $array_feed[$i]['role']= 'deportes';
                $array_feed[$i]['path']= $val['path'];
                $array_feed[$i]['keywords']= $val['keywords'];
                $array_feed[$i]['thumbnail']= $thumb;
                $array_feed[$i]['still']= $still;
                $array_feed[$i]['programid']= "0129";
                $array_feed[$i]['guid']= $val['guid'];
                $i++;
            }
        }
    }
}
if($deportes_app_car['items'] != null || $deportes_app['items'] != ""){
    foreach($deportes_app_car['items'] as $key => $val){
        if(isset($val['urls'][$formatvid]) && $val['urls'][$formatvid] != ""){
            $url_video= $val['urls'][$formatvid];
            $ur_vid_aux= explode("?", $url_video);
            if(count($ur_vid_aux > 1)){
                $url_video= $ur_vid_aux[0];
            }
            $url_video= strtolower($url_video);
            $url_video= str_replace($subdominio, 'apps.',$url_video);

            if(isset($val['description']) && $val['description'] != ""){
                $description= $val['description'];
            }else{
                $description= $val['title'];
            }
            if(isset($val['publidate']) && $val['publidate'] != ""){
                $date_pub= date('Y-m-d H:i:s ',strtotime($val['publidate']));
            }else{
                $date_pub= date("Y-m-d H:i:s");
            }
            if(isset($val['duration']) && $val['duration'] != ""){
                $duration= $val['duration'];
            }else{
                $duration= "10";
            }
            $thumb= $val['thumb'];
            $val_thumb= strpos($thumb, 'http://');
            if($val_thumb === false){
                $thumb= $dominio.$thumb;
            }
            $sepurl_thumb= explode("?", $thumb);            //limpiando url de thumb
            if(count($sepurl_thumb) > 1){
                $thumb= $sepurl_thumb[0];
                $thumb= str_replace($subdominio, 'apps.',$thumb);
            }

            if(isset($val['still']) && $val['still'] != ""){
                $still= $val['still'];
            }else{
                $still= $val['thumb'];
            }
            $val_still= strpos($still, 'http://');
            if($val_still === false){
                $still= $dominio.$still;
            }
            $sepurl_still= explode("?", $still);
            if(count($sepurl_still) > 1){
                $still= $sepurl_still[0];
                $still= str_replace($subdominio, 'apps.',$still);
            }

            $array_feed[$i]['section']= "carousel";
            $array_feed[$i]['ind_feed']= $key;
            $array_feed[$i]['video_id']= $val['id'].$sufijo;
            $array_feed[$i]['title']= $val['title'];
            $array_feed[$i]['link']=  $val['urlpublic'];
            $array_feed[$i]['description']= $description;
            $array_feed[$i]['pubDate']= $date_pub;
            $array_feed[$i]['pubDateTime']= $date_pub;
            $array_feed[$i]['url_video']= $url_video;
            $array_feed[$i]['duration']= $duration;
            $array_feed[$i]['program']= 'deportes';
            $array_feed[$i]['role']= 'deportes';
            $array_feed[$i]['path']= $val['path'];
            $array_feed[$i]['keywords']= $val['keywords'];
            $array_feed[$i]['thumbnail']= $thumb;
            $array_feed[$i]['still']= $still;
            $array_feed[$i]['programid']= "0129";
            $array_feed[$i]['guid']= $val['guid'];
            $i++;
        }
    }
}
//echo "<pre>"; print_r($array_feed); echo "</pre>";die();
header("Content-type: text/xml; charset=ISO-8859-1");
echo '<'.'?xml version="1.0" encoding="ISO-8859-1"?'.'>';
echo "\n";
?>
<resources>
    <?php   if($array_feed != "" || $array_feed != null){
        foreach($array_feed as $key_item => $value_element){
            if($value_element['url_video'] != ""){      ?>
                <resource>
                    <type>video</type>
                    <attributes>
                        <title><![CDATA[<?=chars_specials(utf8_decode($value_element['title']))?>]]></title>
                        <description><![CDATA[<?=chars_specials(utf8_decode($value_element['description']))?>]]></description>
                        <external_id><?=$value_element['video_id']?></external_id>
                        <pubDate><?=$value_element['pubDateTime']?></pubDate>
                        <enabled>true</enabled>
                        <alternative_streams>
                            <default><![CDATA[<?=$value_element['url_video']?>]]></default>
                        </alternative_streams>
                        <image_assets>
                            <image_asset>
                                <key>item_small</key>
                                <url><?=$value_element['thumbnail']?></url>
                            </image_asset>
                            <image_asset>
                                <key>item_big</key>
                                <url><?=$value_element['still']?></url>
                            </image_asset>
                        </image_assets>
                    </attributes>
                </resource>
            <?php           }
        }
    }       ?>
</resources>